﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Alumini.Startup))]
namespace Alumini
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
