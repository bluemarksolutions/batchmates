﻿using Alumini.Interfaces;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alumini.MediatorClass
{
    public class BusinessLayer
    {
        public ICRUDGerneral  CRUDobj;
        
        public BusinessLayer(ICRUDGerneral CRUDobj)
        {
            this.CRUDobj = CRUDobj;
        }
        public List<AskQuestionModel>GetData(string ID, string Mode)
        {
            return CRUDobj.GetData(ID,Mode);
        }               
    }
}