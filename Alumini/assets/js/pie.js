/* ------------------------------------------------------------------------------
 *
 *  # Google Visualization - pie chart
 *
 *  Google Visualization pie chart demonstration
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */


// Pie chart
// ------------------------------

// Initialize chart
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawPie);


// Chart settings    
function drawPie() {
// Pie Chart Region Share - Order Booking Summary - Net value Start

    // Data
    var data = google.visualization.arrayToDataTable([
        ['Sources', 'No. of appointments'],
        ['FTND', 35],
		['LSCS', 20],
        ['FORCEPS', 2],
        ['ABORTION', 3],
		['MTP', 4],
    ]);

    // Options
    var options_pie = {
        fontName: 'Roboto',
        fontSize: 12,
        height: 250,
        width: 380,
        colors: ['#2ec7c9', '#d87a80', '#ffb980', '#909090', '#b49ae5'],
        chartArea: {
            top: 40,
            left: 10,
            width: '100%',
            height: '100%',
        },
        //pieHole: 0.4,
        legend: { position: 'right', alignment: 'center' }
    };
    

    // Instantiate and draw our chart, passing in some options.
    var pie = new google.visualization.PieChart($('#google-pie')[0]);
    pie.draw(data, options_pie);
	
// Pie Chart Region Share - Order Booking Summary - Net value End


}
