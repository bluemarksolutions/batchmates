﻿pageName = '';

$(document).ready(function () {
    
    debugger;
    var str = document.location.href.match(/[^\/]+$/)[0];

    if (str.includes("list")) {
        pageName = "list";
    } else if (str.includes("order")) {
        pageName = "orders";
    }
    if (pageName == "list") {
        GetQuestions();
    }
});

//Validation for whole page in one place
function Universal_Page_Validation() {
    var count = 0;
    var IsValid = "";
    $('.detail-form').find("input[type='text'],select,textarea").each(function () {
        var div = '#' + this.id;
        var span = '#spn' + this.id
        if ($(span).length) {
            if ($(div).val() == "" || $(div).val() == null || $(div).val() == "1") {
                $(span).css("visibility", "visible");
                count++;
            }
            else {
                $(span).css("visibility", "hidden");
            }
        }
    });
    IsValid = count == 0 ? true : false;
    return IsValid;
}

function clearfields() {
    $('#question_title').val('');
    $('#question').val('');
    $("#question_urgency")[0].selectedIndex = 0;
    $("#publish_to")[0].selectedIndex = 0;   
}

function AddNewQuestion() {    
    var question = $('#question_title').val();
    var urgency = $('#question_urgency option:selected').text();
    var quespublish_totion = $('#publish_to option:selected').text(); 
    // question textbox is used as description
    var description = $('#question').val();
    var question_number = "Q20-1";
    var EmpID = $('#hdnEmpID').val();
    var mode = "ADD";
    var returnVal = Universal_Page_Validation();
    if (returnVal) {
        $.ajax({
            url: 'saveaskquestion',
            data: "{'question_number':'" + question_number + "','question':'" + question + "','EmpID':'" + EmpID + "','quespublish_totion':'" + quespublish_totion + "','urgency':'" + urgency + "','description':'" + description + "','mode':'" + mode + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                alert("Success " + data);
            },
            error: function (response) {
                alert("Error");
            },           
        });
    }
    return returnVal;
}



function GetQuestions() {
    debugger
    var ID = 2;
    var mode = "ALL";
    var returnVal = Universal_Page_Validation();
    if (returnVal) {
        $.ajax({
            url: 'getaskquestion',
            data: "{'ID':'" + ID + "','mode':'" + mode + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                alert("Success " + data);
            },
            error: function (response) {
                alert("Error");
            },
        });
    }
    return returnVal;
}