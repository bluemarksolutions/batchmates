﻿function showMessage(className) {
    var csName = "." + className;
    $(csName).show();
    $(csName).css("display", "block");
    $(csName).hide().fadeIn(200).delay(5000).fadeOut("slow", function () {
        $('#autoclosable-btn-success').prop("disabled", false);
    });
}

$(function () {
    dateSelectionChanged();
});

function dateSelectionChanged() {
    $('#ContentPlaceHolder1_txtDOB').change('input propertychange paste', function () {

        var mdate = $(this).val();

        mdate = new Date(mdate);
        //alert(mdate);
        var dayThen = mdate.getDate();
        var monthThen = mdate.getMonth() + 1;
        var yearThen = mdate.getFullYear();

        var today = new Date();
        var birthday = new Date(yearThen, monthThen - 1, dayThen);

        var differenceInMilisecond = today.valueOf() - birthday.valueOf();

        var year_age = Math.floor(differenceInMilisecond / 31536000000);
        var day_age = Math.floor((differenceInMilisecond % 31536000000) / 86400000);

        var month_age = Math.floor(day_age / 30);

        day_age = day_age % 30;

        $('#txtAge').val(year_age + " Yr(s) " + month_age + " Month(s)");

        //alert("You are" + year_age + " years " + month_age + " months " + day_age + " days old");
    });
}

function popupClose() {
    $('#AntenatalRecord').modal('hide');
    $('#updateDischarge').modal('hide');
    $('#myModal').modal('hide');
    $('.bs-example-modal-sm').modal('hide');
    getPrescriptionDataForUpdate($("#hdnPrescriptionStatus").val());
}

function getRowID(rowID, tabName) {
    $("#hdnRowID").val(rowID);
    $("#hdnTabName").val(tabName);
}

$(document).ready(function () {
    // Previous delivery fields 
    $('#btnAddAttachment').click(function () {
        $('#txtWhere').val('');
        $('#txtchildAge').val('')
        document.getElementById("ddlDeliveryType").selectedIndex = 0;
        $('input[type="radio"][value="Male"]').prop('checked', true);
        $('input[type="radio"][value="Alive"]').prop('checked', true);
        $("#hdnPreviousDeliveryRowNumber").val('');
    });

    // Add new room pop up fields
    $('#btnAddRoom').click(function () {
        $('#txtRoomBedNo').val('');
        $('#txtCharges').val('')
        document.getElementById("ddlPopupRoomTypes").selectedIndex = 0;
        document.getElementById("ddlCurrentStatus").selectedIndex = 0;
        $("#hdnNewRoomRowNumber").val('');
    });

    $('#btnAddAntenatalRecord').click(function () {
        $('#txtWeight').val('');
        $('#txtBP').val('');
        $('#txtUterusSize').val('');
        document.getElementById("drpPosition").selectedIndex = 0;
        document.getElementById("drpFHS").selectedIndex = 0;
        document.getElementById("drpEdema").selectedIndex = 0;
        $('#txtAntenatalNotes').val('');
        $('#txtVisitDate').val('');
        $('#txtTT1').val('');
        $('#txtTT2').val('');
        $("#hdnPreviousDeliveryRowNumber").val('');
    });

    $('#btnBookApp').click(function () {
        $('#txtAptDate').val('');
        $('#txtName').val('');
        $('#txtpersonContact').val('');
        $('#txtNotes').val('');
        document.getElementById("drpConsultingDoctor").selectedIndex = 0;
        document.getElementById("drpTimeSlot").selectedIndex = 0;
        document.getElementById("drpPatientype").selectedIndex = 0;
    });


    $('.DatepickerCal').datepicker({
        onSelect: function (date) {
            alert(date)
        },
        defaultDate: "+1w",
        dateFormat: 'dd-MM-yyyy',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        inline: true
    });

    $(".edit-center button").click(function () {
        $('#external-events').css({ "right": "0", "transform": "scaleX(1)" });
        $('form').append('<div class="slide-backdrop"></div>');
    });
});

$(document).on("click", ".slide-backdrop", function () {
    $(this).remove();
    $('#external-events').css({ "right": "-450px" });
});

