﻿_pageName = "";
$(document).ready(function () {
    var str = document.location.href.match(/[^\/]+$/)[0];
    if (str.includes("patient")) {
        _pageName = "patient";
        bindTabCounts();
    }
});

function bindTabCounts() {
    $.ajax({
        url: '/Webservices/GetTabCounts.asmx/getTabCounts',
        data: "{'ID':'" + $("#hdnPatient").val() + "','_pageName': '" + _pageName + "'}",
        dataType: 'json',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: GetCountsOnSuccess,
        error: function (response) {
            alert(response.responseText);
        },
        failure: function (response) {
            alert(response.responseText);
        }
    });
}

function GetCountsOnSuccess(response) {
    var count = response.d;
    if (_pageName.includes("patient")) {
        $(count).each(function () {
            $('.previousDeliveryCount').text(this.split('-')[0]);
            $('.antenatalCount').text(this.split('-')[1]);
            $('.attachmentCount').text(this.split('-')[2]);
        });
    }
}