﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Alumini.BaseClass
{
    public class ConnectionString
    {
        public string currentConnectionString;
        public string attachmentEntity;
        public ConnectionString(string baseURL)
        {
            if (baseURL.ToLower().Contains("batchmates"))
            {
                currentConnectionString = ConfigurationManager.ConnectionStrings["BatchmateConnectionString"].ConnectionString;
            }
            else if (baseURL.ToLower().Contains("test"))
            {
                currentConnectionString = ConfigurationManager.ConnectionStrings["TestCRMConnectionString"].ConnectionString;
            }
            else
            {
                currentConnectionString = ConfigurationManager.ConnectionStrings["BatchmateConnectionString"].ConnectionString;
            }
        }
    }
}