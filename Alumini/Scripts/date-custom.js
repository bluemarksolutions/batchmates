﻿$(document).ready(function () {
    var $table = $('.tablesorter'),
validDate = function (d) {
    return d instanceof Date && isFinite(d);
},
updateFilters = function () {
    var range = [],
        from = $('.fromd').datepicker('getDate'),
        to = $('.tod').datepicker('getDate');
    from = validDate(from) ? from.getTime() : '';
    to = validDate(to) ? to.getTime() : '';
    range[4] = from ? (to ? from + ' - ' + to : '>=' + from) :
        (to ? '<=' + to : '');
    $.tablesorter.setFilters($table, range, true);
};

    $table.tablesorter({
        sortList: [[4, 0]],
        theme: "bootstrap",
        widthFixed: true,
        widgets: ["uitheme", "filter", "zebra"],
        widgetOptions: {
            filter_reset: 'button.reset',
            // hide column filters
            filter_columnFilters: false
        }
    })
    .tablesorterPager({
        container: $("#pager")
    });

    $(".fromd").datepicker({
        defaultDate: "+1w",
        dateFormat: 'd-M-yy',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "minDate", selectedDate);
            setTimeout(function () { updateFilters(); }, 1);
        }
    });

    $(".tod").datepicker({
        defaultDate: "+1w",
        dateFormat: 'd-M-yy',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1,
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "maxDate", selectedDate);
            setTimeout(function () { updateFilters(); }, 1);
        }
    });

    //Added below script for sort by date
    $('tr').remove('.filtered');

    $(".DatepickerCal").datepicker({
        defaultDate: "+1w",
        dateFormat: 'd-M-yy',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 1
    });
});