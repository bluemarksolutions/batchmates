﻿$(document).ready(function () {
    var str = document.location.href.match(/[^\/]+$/)[0];

    if (str.includes("shopping_cart")) {
        // Added 'onClose' event to remove red border around mandatory fields in shopping cart order data tab
        // author:Mukesh(13-Sept-2019)
        $(".DatepickerCal").datepicker({
            defaultDate: "+1w",
            dateFormat: 'd-M-yy',
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            onClose: function (selectDate) {
                if (selectDate != '') {
                    $(this).css('border-color', '');
                }
            }
        });

        $(".fromod").datepicker({
            defaultDate: "+1w",
            dateFormat: 'd-M-yy',
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            yearRange: '-25:+05',
            onClose: function (selectedDate) {
                $("#to").datepicker("option", "minDate", selectedDate);
                setTimeout(function () { updateFilters(); }, 1);
            }
        });

        $(".tood").datepicker({
            defaultDate: "+1w",
            dateFormat: 'd-M-yy',
            changeMonth: true,
            changeYear: true,
            numberOfMonths: 1,
            yearRange: '-25:+05',
            onClose: function (selectedDate) {
                $("#from").datepicker("option", "maxDate", selectedDate);
                setTimeout(function () { updateFilters(); }, 1);
            }
        });

        $("#site_dimensions").change(function () {
            if ($("#site_dimensions option:selected").val() == "Yes") {
                $('#fileuploadcontrol').css("display", "block");
            }
            else {
                $('#fileuploadcontrol').css("display", "none");
            }
        });

        $("#measurement_protocol").change(function () {
            if ($("#measurement_protocol option:selected").val() == "Yes") {
                $('#fileuploadcontrol2').css("display", "block");
            }
            else {
                $('#fileuploadcontrol2').css("display", "none");
            }
        });

        $("#call_for_delivery").change(function () {
            if ($("#call_for_delivery option:selected").val() == "Yes") {
                $('#fileuploadcontrol3').css("display", "block");
            }
            else {
                $('#fileuploadcontrol3').css("display", "none");
            }
        });
    }
});
//below function use to display message for three second
function showmsg(cs_Name) {
    var csName = "." + cs_Name;
    $(csName).show();
    $(csName).css("display", "block");
    $(csName).hide().fadeIn(200).delay(3000).fadeOut("slow", function () {
        $('#autoclosable-btn-success').prop("disabled", false);
    });
}

//below funtion use to validate email id
function emailValidate(email) {
    var check = "" + email;
    if ((check.search('@') >= 0) && (check.search(/\./) >= 0))
        if (check.search('@') < check.split('@')[1].search(/\./) + check.search('@'))
            return true;
        else return false;
    else return false;
}

//Add commas to numeric values
function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    //x2 = x.length > 1 ? '.' + x[1] : '';
    x2 = x.length > 1 ? '' : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

// Allow users to add only numeric values in input box
// author:Mukesh(25-Jul-2019)
function isNumeric(keyCode) {
    return ((keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 9 || keyCode == 46)
}

// Selecting file from open dialogue box to upload it
// author:Mukesh(11-Oct-2019)
function selectFile(name, fileBrowseNo) {
    var fileBrowseId = "#file_uploader" + fileBrowseNo;
    var fileSpanName = "#upload-file-info" + fileBrowseNo;

    var Fname = "";
    var files = $(fileBrowseId)[0].files;

    if (files.length > 1) {
        $(".AttachName").css("display", "none");
    } else {
        $(".AttachName").css("display", "block");
    }

    $(fileSpanName).html("");

    for (var i = 0; i < files.length; i++) {
        var div = $("<span class='label label-default' style='margin-top: 4px;'>" + files[i].name + "</span>");
        $(fileSpanName).append(div);
        $(fileSpanName).append("&nbsp;&nbsp;");
    }

    $(fileSpanName).css("display", "inline-block");
    $("#upload-file-chk").css("display", "none");
    return false;
    //*****Old code
}

// Upload Selected Files
// author:Mukesh(11-Oct-2019)
function uploadFile(sequential_Id) {

    var uploadedFile = $("#file_uploader1").get(0);
    var files = uploadedFile.files;

    var uploadedFile2 = $("#file_uploader2").get(0);
    var files2 = uploadedFile2.files;

    var uploadedFile3 = $("#file_uploader3").get(0);
    var files3 = uploadedFile3.files;

    var fileName = "";

    if (uploadedFile.files.length > 0 || uploadedFile2.files.length > 0 || uploadedFile3.files.length > 0) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var files_data = new FormData();

            if (uploadedFile.files.length > 0) {
                for (var i = 0; i < files.length; i++) {
                    fileName = files[i].name;
                    files_data.append(fileName + "@" + sequential_Id, files[i]);
                }
            }

            if (uploadedFile2.files.length > 0) {
                for (var i = 0; i < files2.length; i++) {
                    fileName = files2[i].name;
                    files_data.append(fileName + "@" + sequential_Id, files2[i]);
                }
            }

            if (uploadedFile3.files.length > 0) {
                for (var i = 0; i < files3.length; i++) {
                    fileName = files3[i].name;
                    files_data.append(fileName + "@" + sequential_Id, files3[i]);
                }
            }

            $.ajax({
                url: "/home/UploadFile",
                type: "POST",
                data: files_data,
                contentType: false,
                processData: false,
                success: function (result) {
                },
                error: function (err) {
                    $(".div2").css("display", "none");
                }
            });
        }

        if (uploadedFile.files.length > 0) {
            reader.readAsDataURL(uploadedFile.files[0]);
        }
        if (uploadedFile2.files.length > 0) {
            reader.readAsDataURL(uploadedFile2.files[0]);
        }
        if (uploadedFile3.files.length > 0) {
            reader.readAsDataURL(uploadedFile3.files[0]);
        }
    }
}









