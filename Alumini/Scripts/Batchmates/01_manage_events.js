﻿function SaveEvent(page_name) {
    debugger;
    var returnValue = Validation_DetailsTab(page_name);

    if (returnValue) {
        var itemlist = new Array();
        var item = {
            event_title: $('#event_title').val(), who_can_see: $('#who_can_see option:selected').val(),
            start_date: $('#start_date').val(), end_date: $('#end_date').val(), event_description: $('#event_description').val()
        }

        itemlist.push(item);

        // call ajax to save the data
        $.ajax({
            url: "SaveEvent",
            dataType: "html",
            data: JSON.stringify(itemlist),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (returnValue) {

                if ($.parseJSON(returnValue) == 'saved') {
                    $('#mySmallModalLabel').html("<i class='icon-check' style='color:#06d06d;margin-top:3px;'></i> &nbsp; Event Saved");
                    $('#alert_message').html('Event has been successfully saved & a notification email has been sent to everyone');
                } else {
                    $('#mySmallModalLabel').html("<i class='icon-close' style='color:red;margin-top:3px;'></i> &nbsp;Error Saving Event");
                    $('#alert_message').html('Something went wrong while saving the event. Please try again.');
                }

                $('#message').modal('show');

                $('#new_event').modal('hide');

                //clears the event pop up fields function
                clear_event_pop_up_fields();
            },
            error: function (xhr) {
                alert(JSON.stringify(xhr));
                $(".div2").css("display", "none");
            }
        });
    }
}

function clear_event_pop_up_fields() {
    $('#event_title').val('');
    $('#event_description').val('');
    $('#start_date').val('');
    $('#end_date').val('');

}