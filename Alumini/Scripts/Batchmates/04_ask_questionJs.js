﻿var pageName = '';

$(document).ready(function () {

    var str = document.location.href.match(/[^\/]+$/)[0];
    if (str.includes("list")) {
        pageName = "list";
    } else if (str.includes("order")) {
        pageName = "orders";
    }
    if (pageName == "list") {
        GetQuestions('OTHER');
    }

    $('.AddAnswer').click(function () {
        debugger
        var answer = $('#AnswerByUser').val();       
        var QuestionAskBy = $('#hdnAskBy').val();
        var AnswerGivenBy = $('#hdnEmpID').val();
        var question_number = $('#hdnQuestionNumber').val();
        var question_id = $('#hdnQuestionID').val();
        var returnVal = Question_Validation();
       
        if (returnVal) {  
        var itemlist = new Array();
        var item = {
            answer: answer, question_ask_by: QuestionAskBy,
            answer_given_by: AnswerGivenBy, question_number: question_number, question_id: question_id
        }
        itemlist.push(item);
        $.ajax({
            url: 'savegivenanswer',
            data: JSON.stringify(itemlist),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //alert("Success " + $.parseJSON(data));
                displayMessage("Answer post Successfully.", "success");
            },
            error: function (response) {
                alert("Error");
            },
        });
        }
        return returnVal;
    });
});

//Validation for whole page in one place
function Universal_Page_Validation() {
    var count = 0;
    var IsValid = "";
    $('.detail-form').find("input[type='text'],select,textarea").each(function () {        
        var div = '#' + this.id;
        var span = '#spn' + this.id
        if ($(span).length) {
            if ($(div).val() == "" || $(div).val() == null || $(div).val() == "1") {
                $(span).css("visibility", "visible");
                count++;
            }
            else {
                $(span).css("visibility", "hidden");
            }
        }
    });
    IsValid = count == 0 ? true : false;
    return IsValid;
}

function clearfields() {
    GenerateAutoNumber();
    $('#question_title').val('');
    $('#question').val('');
    $("#question_urgency")[0].selectedIndex = 0;
    $("#publish_to")[0].selectedIndex = 0;
}

function GenerateAutoNumber() {
    $.ajax({
        url: 'GenNum',        
        dataType: "json",
        type: "GET",
        contentType: "application/json; charset=utf-8",
        success: function (data) { $('#hdnAutoID').val(data);},
        error: function (response) {},});
}

function AddNewQuestion() {
    var question = $('#question_title').val();
    var urgency = $('#question_urgency option:selected').text();
    var quespublish_totion = $('#publish_to option:selected').text();
    // question textbox is used as description
    var description = $('#question').val();   
    var question_number = $('#hdnAutoID').val();
    var EmpID = $('#hdnEmpID').val();
    var mode = "ADD";
    var returnVal = Universal_Page_Validation();
    if (returnVal) {

        var itemlist = new Array();
        var item = {
            question_number: question_number, question: question,
            userid: EmpID, publish_to: quespublish_totion, urgency: urgency, description: description, mode: mode
        }
        itemlist.push(item);
        $.ajax({
            url: 'saveaskquestion',
            data: JSON.stringify(itemlist),
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                alert("Success " + $.parseJSON(data));
            },
            error: function (response) {
                alert("Error");
            },
        });
    }
    return returnVal;
}

function GetQuestions(mode) {
    var ID = $('#hdnEmpID').val();
    var mode = mode;
     $.ajax({
        url: 'getaskquestion',
        data: "{'ID':'" + ID + "','mode':'" + mode + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            DisplayQuestions(data);
        },
        error: function (response) {
        },
    });    
}

function DisplayQuestions(data) {
    var row = '';
    $("#borderBox_tab1").empty();
    $(data).each(function () {
        var created_on = new Date(parseInt(this.displaycreatedate.substr(6)));
        row += "<div class='card'>"
        row += "<div class='card-body'>"
        row += "<div class='bm-question-card mb-15'>"
        row += "<div class='row'>"
        row += " <div class='col-sm-12'>"
        row += "<div class='pull-left pr-10'>"
        row += "<img alt='user' class='img-circle' src='../assets/img/user1.svg' width='40'></div>"
        row += " <div class='pull-left'>"
        row += "<a href='#'><label class='f-16'><b>" + this.question_number + ": &nbsp; " + this.question + "?</b></label></a><br />"
        row += "<div style='font-size:12px;'>"
        row += "<span class='text-muted'>"
        row += " " + this.description + "</span>"        
        row += "</div>"
        row += "</div>"
        row += "<div class='pull-right pb-0'>"
        row += "<button type='button' id='btnAnsQuestion' onclick=getQuestionData(" + this.quisttion_id + ");  class='btn btn-default btn-animated btn-sm pr-40' data-toggle='modal' data-target='#answer_question'>Answer<i class='icon-plus3 text-white f-12'></i></button>"
        row += "<button type='button' id='btnReportQuestion' class='btn btn-danger btn-animated btn-sm pr-40' data-toggle='modal' data-target='#answer'>Report<i class='icon-plus3 text-white f-12'></i></button>"
        row += " </div>"
        row += " </div>"
        row += "</div>"
        row += " <hr />"
        row += "<div class='row'>"
        row += "<div class='col-sm-8 pull-left'>"
        row += "<div class='text-muted pl-20'>"
        row += "<span class='pull-left pr-15 text-info'><i class='fa fa-check'></i> In Progress</span>"
        row += "<span class='pull-left pr-15'><i class='fa fa-comment'></i> " + this.answer_count + " Answers</span>"
        row += "<span class='pull-left pr-15'><i class='fa fa-eye'></i> " + this.view_count + " Views</span>"
        row += "<span class='pull-left pr-15 text-danger'><i class='fa fa-bullhorn'></i> " + this.urgency + " </span>"
        row += "<span class='pull-left pr-15 text-info'><i class='fa fa-globe'></i> " + this.publish_to + " </span>"
        row += "</div>"
        row += "</div>"
        row += "<div class='col-sm-4 pull-right'>"
        row += "<span class='pull-right pr-15 f-10 text-muted'>Created On: " + (this.displaycreatedate) + " </span>"
        row += "<span class='pull-right pr-15 f-10 text-muted'>Created by: " + this.created_by + " </span>"
        row += "</div>"
        row += "</div>"
        row += "</div>"
        row += "</div>"
        row += "</div>";
    });
    $('#borderBox_tab1').append(row);
}

function getQuestionData(QuestionNumber){
   var ID = $('#hdnEmpID').val();
    var QNum = QuestionNumber;
    var count = 0;
    var row = "";
    $(".allanswers").empty();
    $.ajax({
        url: 'GetQuestionWithReplyAnswer',
        data: "{'ID':'" + ID + "','QNum':'" + QNum + "'}",
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {           
            $(data).each(function () {   
                count++;
                if (count == 1) {
                    $('#QuestionNumber').text(this.question_number);
                    $('#QuestionDetail').text(this.question);
                    $('#hdnQuestionID').val(this.quisttion_id);
                    $('#hdnAskBy').val(this.created_by_id);
                    $('#hdnQuestionNumber').val(this.question_number);
                }
                 if (this.count != 0) {
                    row += "<li><div class='avatar'><img src='../assets/img/user1.svg' alt=''></div>";                   
                    row += "<div class='activity-desk'>";
                    row += " <h5><a href=''><span id='AnswerBy'>" + this.answer_given_by + "</span></a> <span id='AnswerTime'>" + this.answer_given_on + "</span></h5>";                   
                    row += "<p class='text-muted'><b>" + this.answer_given_by_id + " :</b>" + this.answer + ".</p>";
                    row += "</div></li>";                   
                }
            })
            $('.allanswers').append(row);
        },
        error: function (response) {
        },
    });
   
}

function Question_Validation() {
    var IsValid = false;
    var IsEmpty = $('#AnswerByUser').val();
    if (IsEmpty == "") {
        $('#AnswerByUser').css('border-color', 'red');
        $('#spnAnswerByUser').text('Write something before post answer..!');
        IsValid= false;
    }
    else {
        $('#AnswerByUser').css('border-color', '');
        $('#spnAnswerByUser').text('');
        IsValid= true;
    }
    return IsValid;
}