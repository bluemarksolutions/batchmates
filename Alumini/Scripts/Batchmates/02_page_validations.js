﻿_pageName = "";
$(document).ready(function () {
   
    var str = document.location.href.match(/[^\/]+$/)[0];

    if (str.includes("userprofile")) { //administration/userprofile
        _pageName = "userprofile";
    } else if (str.includes("begin_password_reset")) {//account/begin_password_reset
        _pageName = "begin_password_reset";
    }
    //For TextBox
    $('input[validationTabDetails="validation"],[validationTabDetails="email"]').bind('keyup', function () {
        if ($(this).val() == "") {
            $(this).css('border-color', 'red');
        } else {
            $(this).css('border-color', '');
        }
    });

    //For DropDown
    $('select[validationTabDetails="validation"],[validationSetting="validation"],[validationVisitors="validation"],[validationVisitorsSch="validation"],[validationLocationDetails="validation"]').bind('change', function () {
        if ($(this).val() == "") {
            $(this).next().css("border", "1px solid red");
        } else {
            $(this).next().css('border-color', '');
            $(this).next().css('border-width', '0');
            $(this).next().css("border", "0px");
        }
    });
});

function ValidationEmail_DetailsTab(Flag) {
    var isVaild = true;
    if (Flag == "TabDetails") {
        //For Email TextBox
        $('input[validationTabDetails="email"]').each(function () {
            if ($(this).val() == "") {
                $(this).css('border-color', 'red');
                isVaild = false;
            } else {
                isVaild = emailValidate($(this).val());// emailValidate() function from Comman_funtion.js
                if (!isVaild) {
                    $('#emailValidation').text('Please enter a valid email address.');
                } else {
                    $('#emailValidation').text('');
                }
            }
        });
    }
    else if (Flag == "DealerSettingTab") {
        //For Email TextBox
        $('input[validationTabSettings="email"]').each(function () {
            if ($(this).val() == "") {
                $(this).css('border-color', 'red');
                isVaild = false;
            } else {
                isVaild = emailValidate($(this).val());// emailValidate() function from Comman_funtion.js
                if (!isVaild) {
                    $('#settingEmailValidation').text('Please enter a valid email address.');
                } else {
                    $('#settingEmailValidation').text('');
                }
            }
        });
    }
    return isVaild;
}

function Validation_DetailsTab(Flag) {
    var isVaild = true;
    if (Flag == "events") {
        $('input[validationTabDetails="validation"]').each(function () {
            if ($(this).val() == "") {
                $(this).css('border-color', 'red');
                isVaild = false;
            }
            else {
                $(this).css('border-color', '');
            }
        });

        // Check 'Site Dimension' dropdown

        $('select[validationTabDetails="validation"]').each(function () {
            if ($(this).val() == "0" || $(this).val() == null) {
                $(this).next().css("border", "1px solid red");
                isVaild = false;
            }
            else {
                $(this).next().css('border-color', '');
            }
        });
    }
    else if (Flag == "register") {

        $('input[validationTabDetails="validation"]').each(function () {
            if ($(this).val() == "") {
                $(this).css('border-color', 'red');
                isVaild = false;
            }
            else {
                $(this).css('border-color', '');
            }
        });

        // Check 'Site Dimension' dropdown

        $('select[validationTabDetails="validation"]').each(function () {
            if ($(this).val() == "0" || $(this).val() == null) {
                $(this).next().css("border", "1px solid red");
                isVaild = false;
            }
            else {
                $(this).next().css('border-color', '');
            }
        });
    }
    else if (Flag == "CheckUserName") {
        $('input[validationTabDetails="popupUserName"]').each(function () {
            if ($(this).val() == "") {
                $(this).css('border-color', 'red');
                isVaild = false;
            }
        });
    }
    return isVaild;
}