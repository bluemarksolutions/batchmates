﻿
$(document).ready(function () {
    var str = document.location.href.match(/[^\/]+$/)[0];

    if (str.includes('verifyaccount')) {
        verify_account(str);
    }
});

function alumni_registraion(page_name) {
    var returnValue = Validation_DetailsTab(page_name);
    debugger
    if (returnValue) {
        var itemlist = new Array();
        var item = {
            first_name: $('#first_name').val(), last_name: $('#last_name').val(),
            contact_number: $('#contact_number').val(), email: $('#email').val(), password: $('#password').val(), batch: $('#batch').val()
        }

        itemlist.push(item);

        // call ajax to save the data
        $.ajax({
            url: "SaveRegistration",
            dataType: "html",
            data: JSON.stringify(itemlist),
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if ($.parseJSON(result) == 'email_exist') {
                    $('#mySmallModalLabel').html("<i class='icon-close' style='color:red;margin-top:3px;'></i> &nbsp;Registration Error");
                    $('#alert_message').html('Email already registered with Batchmate, kindly try with different email id');
                }
                else if ($.parseJSON(result) == 'saved') {

                    setTimeout(function () {
                        sendemail($('#email').val());
                    }, 1000);

                    $('#mySmallModalLabel').html("<i class='icon-check' style='color:#06d06d;margin-top:3px;'></i> &nbsp;Registration Successful");
                    $('#alert_message').html('<b>Thank You!</b> An email has been sent to your registered email id, kindly verify and login your account.');
                }

                $('#message').modal('show');
            },
            error: function (xhr) {
                alert(JSON.stringify(xhr));
                $(".div2").css("display", "none");
            }
        });
    }
}

function sendemail(email_id) {
    $.ajax({
        url: "SendEmail",
        dataType: "html",
        data: JSON.stringify({ emailid: email_id, firstname: $('#first_name').val(), lastname: $('#last_name').val() }),
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (result) {

        },
        error: function (xhr) {
            $(".div2").css("display", "none");
        }
    });
}

// After registration user clicks on OK button it refreshes the page.
function refreshPage() {
    window.location.href = "/authentication/register";
}

function verify_account(url) {
    var alumnus_email = url.split('=')[1];

    $.ajax({
        url: "VerifyAlumnus",
        dataType: "html",
        data: JSON.stringify({ email: alumnus_email }),
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if ($.parseJSON(result) == 'updated') {
                $('#verificationMessage').html("<b>Email address successfully verified. Now, sign in with verified credentials.</b>");
            }
        },
        error: function (xhr) {
            $(".div2").css("display", "none");
        }
    });
}