﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web;

namespace Alumini.Utilities
{
    public class Utilities
    {
        public static bool IsEmptyOrNull(string _Key)
        {
            return _Key != string.Empty && _Key != null && _Key != "0";
        }
        public static bool IsEmptyOrNull(int? _Key)
        {
            return _Key != 0;
        }
        public static string EncodePassword(string password)
        {
            password = "batchmates#" + password;
            var provider = new SHA256CryptoServiceProvider();                          // Hashing with SHA256 algorithm
            var encoding = new UnicodeEncoding();
            return Convert.ToBase64String(provider.ComputeHash(encoding.GetBytes(password)));
        }

        public static string SendEmail(string Email, string Subject, string Message)
        {
            if (IsEmptyOrNull(Email))
            {
                MailMessage message = new MailMessage();
                SmtpClient Mail = new SmtpClient();
                message.Bcc.Add(Email.Trim());
                message.IsBodyHtml = true;
                message.Subject = Subject;
                message.Body = Message;

                try
                {
                    // Send the email in the background thread using Thread object of Threading Class. author:Mukesh(21-Aug-2019)
                    Thread email = new Thread(delegate ()
                    {
                        Mail.Send(message);
                    });

                    email.IsBackground = true;
                    email.Start();
                    return "true";
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    return "false";
                }
            }
            else
            {
                return "false";
            }
        }
    }
}