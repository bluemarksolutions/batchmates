﻿using Alumini.BaseClass;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.DAL
{
    public class ManageEvent : ConnectionString
    {
        public ManageEvent(string absoluteURL) : base(absoluteURL)
        {

        }

        SqlConnection con = null;

        #region Events
        public string SaveEvent(AlumniEvent oEvent)
        {
            string returnValue = string.Empty;
            // CreatedBy variable holds the logged in user name
            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand cmd = new SqlCommand("03_alumni_events", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@event_title", oEvent.event_title);
                    cmd.Parameters.AddWithValue("@who_can_see", oEvent.who_can_see);
                    cmd.Parameters.AddWithValue("@start_date", oEvent.start_date);
                    cmd.Parameters.AddWithValue("@end_date", oEvent.end_date);
                    cmd.Parameters.AddWithValue("@created_by", oEvent.created_by);
                    cmd.Parameters.AddWithValue("@description", oEvent.event_description);
                    cmd.Parameters.AddWithValue("@mode", "save");
                    con.Open();

                    // ExecuteSqlCommand is custom class executes the sql query and returns the string variable
                    // uses ExecuteScalar method to execute the query
                    //author: Mukesh(25-May-2020)
                    return returnValue = ExecuteSqlCommand.ExecuteQuery(cmd);
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }
        #endregion
    }
}