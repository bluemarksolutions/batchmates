﻿using Alumini.BaseClass;
using Alumini.Interfaces;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.DAL
{
    public class AskQuestionDL : ConnectionString, ICRUDGerneral
    {
        List<AskQuestionModel> IList = new List<AskQuestionModel>();
        AskQuestionModel obj;
        public AskQuestionDL(string absoluteURL) : base(absoluteURL)
        {

        }

        SqlConnection con = null;
        //string connectionstring = ConfigurationManager.ConnectionStrings["AlumniConnection"].ConnectionString;
        //public string saveaskquestion1(string question_number, string question, string EmpID, string quespublish_totion, string urgency, string description, string mode)
        //{            
        //    string result = "";            
        //    using (SqlConnection conn = new SqlConnection())
        //    {
        //        try
        //        { 
        //            conn.ConnectionString = connectionstring;
        //            using (SqlCommand Command = new SqlCommand("sp_save_update_ask_question", conn))
        //            {
        //                Command.CommandType = CommandType.StoredProcedure;
        //                Command.Parameters.AddWithValue("@question_number", question_number);
        //                Command.Parameters.AddWithValue("@mode", mode);
        //                Command.Parameters.AddWithValue("@question", question);
        //                Command.Parameters.AddWithValue("@urgency", urgency);
        //                Command.Parameters.AddWithValue("@quespublish_totion", quespublish_totion);
        //                Command.Parameters.AddWithValue("@description", description);
        //               // Command.Parameters.AddWithValue("@qustion_status", qustion_status);                        
        //                Command.Parameters.AddWithValue("@userid", EmpID);                        
        //                Command.Parameters.AddWithValue("@created_by", EmpID);
        //                Command.Parameters.AddWithValue("@created_on", DateTime.Now);
        //                Command.Parameters.AddWithValue("@modified_by", EmpID);
        //                Command.Parameters.AddWithValue("@modified_on", DateTime.Now);
        //                Command.Connection = conn;
        //                conn.Open();
        //                result = Command.ExecuteScalar().ToString();
        //            }                  

        //        }
        //        catch (Exception ex)
        //        {
        //            string x = ex.Message;
        //            throw ex;                    
        //        }
        //        finally
        //        {
        //            conn.Close();
        //        }
        //    }
        //    return result;            
        //}


        public string saveaskquestion(string question_number, string question, string EmpID, string quespublish_totion, string urgency, string description, string mode)
        {
            string returnValue = string.Empty;

            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand Command = new SqlCommand("sp_save_update_ask_question", con);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.AddWithValue("@question_number", question_number);
                    Command.Parameters.AddWithValue("@mode", mode);
                    Command.Parameters.AddWithValue("@question", question);
                    Command.Parameters.AddWithValue("@urgency", urgency);
                    Command.Parameters.AddWithValue("@quespublish_totion", quespublish_totion);
                    Command.Parameters.AddWithValue("@description", description);
                    // Command.Parameters.AddWithValue("@qustion_status", qustion_status);                        
                    Command.Parameters.AddWithValue("@userid", EmpID);
                    Command.Parameters.AddWithValue("@created_by", EmpID);
                    Command.Parameters.AddWithValue("@created_on", DateTime.Now);
                    Command.Parameters.AddWithValue("@modified_by", EmpID);
                    Command.Parameters.AddWithValue("@modified_on", DateTime.Now);
                    con.Open();

                    returnValue = Command.ExecuteScalar().ToString();
                    return returnValue;
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }


        public string saveaskquestion(AskQuestionModel lParam)
        {
            string returnValue = string.Empty;

            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand Command = new SqlCommand("sp_save_update_ask_question", con);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.AddWithValue("@question_number", lParam.question_number);
                    Command.Parameters.AddWithValue("@mode", lParam.mode);
                    Command.Parameters.AddWithValue("@question", lParam.question);
                    Command.Parameters.AddWithValue("@urgency", lParam.urgency);
                    Command.Parameters.AddWithValue("@quespublish_totion", lParam.publish_to);
                    Command.Parameters.AddWithValue("@description", lParam.description);
                    // Command.Parameters.AddWithValue("@qustion_status", qustion_status);                        
                    Command.Parameters.AddWithValue("@userid", lParam.userid);
                    Command.Parameters.AddWithValue("@created_by", lParam.userid);
                    Command.Parameters.AddWithValue("@created_on", DateTime.Now);
                    Command.Parameters.AddWithValue("@modified_by", lParam.userid);
                    Command.Parameters.AddWithValue("@modified_on", DateTime.Now);
                    con.Open();

                    returnValue = Command.ExecuteScalar().ToString();
                    return returnValue;
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }

        public string saveanswers(Answer lAnswer)
        {
            string returnValue = string.Empty;
            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand Command = new SqlCommand("sp_save_answer_against_question", con);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.AddWithValue("@question_number", lAnswer.question_number);                   
                    Command.Parameters.AddWithValue("@answer", lAnswer.answer);
                    Command.Parameters.AddWithValue("@question_id", lAnswer.question_id);
                    Command.Parameters.AddWithValue("@question_ask_by", lAnswer.question_ask_by);
                    Command.Parameters.AddWithValue("@answer_given_by", lAnswer.answer_given_by); 
                    Command.Parameters.AddWithValue("@created_by", lAnswer.answer_given_by);
                    Command.Parameters.AddWithValue("@created_on", DateTime.Now);                   
                    con.Open();
                    returnValue = Command.ExecuteScalar().ToString();
                    return returnValue;
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }
        public List<AskQuestionModel> GetQuestionWithReplyAnswer(string ID,string QNum)
        {
            List<AskQuestionModel> QList=new  List<AskQuestionModel>();
            AskQuestionModel obj;
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = this.currentConnectionString;
                    using (SqlCommand Command = new SqlCommand("get_question_with_reply_nswer", conn))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.AddWithValue("@ID", ID);
                        Command.Parameters.AddWithValue("@QNum", QNum);
                        Command.Connection = conn;
                        conn.Open();
                        using (SqlDataReader SDReader = Command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            try
                            {
                                while (SDReader.Read())
                                {
                                    obj = new AskQuestionModel();
                                    obj.question = Convert.ToString(SDReader["question"]);
                                    obj.question_number = Convert.ToString(SDReader["question_number"]);
                                    obj.quisttion_id = Convert.ToInt32(SDReader["quisttion_id"]);
                                    obj.qustion_status = Convert.ToString(SDReader["qustion_status"]);
                                    obj.answer_count = Convert.ToString(SDReader["answer_count"]);
                                    obj.description = Convert.ToString(SDReader["description"]);
                                    obj.publish_to = Convert.ToString(SDReader["publish_to"]);
                                    obj.urgency = Convert.ToString(SDReader["urgency"]);
                                    obj.view_count = Convert.ToString(SDReader["view_count"]);
                                    obj.userid = Convert.ToString(SDReader["userid"]);
                                    obj.created_by = Convert.ToString(SDReader["created_by"]);
                                    obj.created_by_id = Convert.ToString(SDReader["created_by_id"]);
                                    obj.created_on = Convert.ToDateTime(SDReader["created_on"]);
                                    obj.displaycreatedate = Convert.ToDateTime(SDReader["created_on"]).ToString("d-MMM-yy");
                                    obj.modified_by = Convert.ToString(SDReader["modified_by"]);
                                    obj.modified_on = Convert.ToDateTime(SDReader["modified_on"]);

                                    obj.answer = Convert.ToString(SDReader["answer"]);
                                    obj.answer_given_by = Convert.ToString(SDReader["answer_given_by"]);
                                    obj.answer_given_on = Convert.ToDateTime(SDReader["answer_given_on"]).ToString("d-MMM-yy");
                                    obj.answer_given_by_id = Convert.ToString(SDReader["answer_given_by_id"]);
                                    obj.count = Convert.ToInt32(SDReader["count"]);
                                    IList.Add(obj);
                                }
                            }
                            catch (Exception ex)
                            { throw ex; }
                            finally
                            { SDReader.Close(); }
                        }
                    }

                }
                catch (Exception ex)
                {
                    string x = ex.Message;
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
            return IList;


        }



        //public List<AskQuestionModel> getaskquestion(string ID, string mode)
        //{
        //    AskQuestionModel obj;
        //    List<AskQuestionModel> IList = new List<AskQuestionModel>();
        //    using (SqlConnection conn = new SqlConnection())
        //    {
        //        try
        //        {
        //            conn.ConnectionString = this.currentConnectionString;
        //            using (SqlCommand Command = new SqlCommand("sp_get_ask_question", conn))
        //            {
        //                Command.CommandType = CommandType.StoredProcedure;
        //                Command.Parameters.AddWithValue("@ID", ID);
        //                Command.Parameters.AddWithValue("@mode", mode);
        //                Command.Connection = conn;
        //                conn.Open();
        //                using (SqlDataReader SDReader = Command.ExecuteReader(CommandBehavior.CloseConnection))
        //                {
        //                    try
        //                    {
        //                        while (SDReader.Read())
        //                        {
        //                            obj = new AskQuestionModel();
        //                            obj.question = Convert.ToString(SDReader["question"]);
        //                            obj.question_number = Convert.ToString(SDReader["question_number"]);
        //                            obj.quisttion_id = Convert.ToInt32(SDReader["quisttion_id"]);
        //                            obj.qustion_status = Convert.ToString(SDReader["qustion_status"]);
        //                            obj.answer_count = Convert.ToString(SDReader["answer_count"]);
        //                            obj.description = Convert.ToString(SDReader["description"]);
        //                            obj.publish_to = Convert.ToString(SDReader["publish_to"]);
        //                            obj.urgency = Convert.ToString(SDReader["urgency"]);
        //                            obj.view_count = Convert.ToString(SDReader["view_count"]);
        //                            obj.userid = Convert.ToString(SDReader["userid"]);
        //                            obj.created_by = Convert.ToString(SDReader["created_by"]);
        //                            obj.created_on = Convert.ToDateTime(SDReader["created_on"]);
        //                            obj.displaycreatedate = Convert.ToString(SDReader["created_on"]);
        //                            obj.modified_by = Convert.ToString(SDReader["modified_by"]);
        //                            obj.modified_on = Convert.ToDateTime(SDReader["modified_on"]);

        //                            IList.Add(obj);
        //                        }
        //                    }
        //                    catch (Exception ex)
        //                    { throw ex; }
        //                    finally
        //                    { SDReader.Close(); }
        //                }
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            string x = ex.Message;
        //            throw ex;
        //        }
        //        finally
        //        {
        //            conn.Close();
        //        }
        //    }
        //    return IList;
        //}



        //public string SaveAndUpdate(string question_number, string question, string EmpID, string quespublish_totion, string urgency, string description, string mode)
        //{
        //    string returnValue = string.Empty;

        //    try
        //    {
        //        con = new SqlConnection(this.currentConnectionString);
        //        using (con)
        //        {
        //            SqlCommand Command = new SqlCommand("sp_save_update_ask_question", con);
        //            Command.CommandType = CommandType.StoredProcedure;
        //            Command.Parameters.AddWithValue("@question_number", question_number);
        //            Command.Parameters.AddWithValue("@mode", mode);
        //            Command.Parameters.AddWithValue("@question", question);
        //            Command.Parameters.AddWithValue("@urgency", urgency);
        //            Command.Parameters.AddWithValue("@quespublish_totion", quespublish_totion);
        //            Command.Parameters.AddWithValue("@description", description);
        //            // Command.Parameters.AddWithValue("@qustion_status", qustion_status);                        
        //            Command.Parameters.AddWithValue("@userid", EmpID);
        //            Command.Parameters.AddWithValue("@created_by", EmpID);
        //            Command.Parameters.AddWithValue("@created_on", DateTime.Now);
        //            Command.Parameters.AddWithValue("@modified_by", EmpID);
        //            Command.Parameters.AddWithValue("@modified_on", DateTime.Now);
        //            con.Open();

        //            returnValue = Command.ExecuteScalar().ToString();
        //            return returnValue;
        //        }
        //    }
        //    catch (Exception EX)
        //    {
        //        return returnValue;
        //    }
        //    finally
        //    {
        //        con.Close();
        //    }

        //}

        public List<AskQuestionModel> GetData(string ID, string mode)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                try
                {
                    conn.ConnectionString = this.currentConnectionString;
                    using (SqlCommand Command = new SqlCommand("sp_get_ask_question", conn))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.AddWithValue("@ID", ID);
                        Command.Parameters.AddWithValue("@mode", mode);
                        Command.Connection = conn;
                        conn.Open();
                        using (SqlDataReader SDReader = Command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            try
                            {
                                while (SDReader.Read())
                                {
                                    obj = new AskQuestionModel();
                                    obj.question = Convert.ToString(SDReader["question"]);
                                    obj.question_number = Convert.ToString(SDReader["question_number"]);
                                    obj.quisttion_id = Convert.ToInt32(SDReader["quisttion_id"]);
                                    obj.qustion_status = Convert.ToString(SDReader["qustion_status"]);
                                    obj.answer_count = Convert.ToString(SDReader["answer_count"]);
                                    obj.description = Convert.ToString(SDReader["description"]);
                                    obj.publish_to = Convert.ToString(SDReader["publish_to"]);
                                    obj.urgency = Convert.ToString(SDReader["urgency"]);
                                    obj.view_count = Convert.ToString(SDReader["view_count"]);
                                    obj.userid = Convert.ToString(SDReader["userid"]);
                                    obj.created_by = Convert.ToString(SDReader["created_by"]);
                                    obj.created_on = Convert.ToDateTime(SDReader["created_on"]);
                                    obj.displaycreatedate = Convert.ToDateTime(SDReader["created_on"]).ToString("d-MMM-yy");
                                    obj.modified_by = Convert.ToString(SDReader["modified_by"]);
                                    obj.modified_on = Convert.ToDateTime(SDReader["modified_on"]);

                                    IList.Add(obj);
                                }
                            }
                            catch (Exception ex)
                            { throw ex; }
                            finally
                            { SDReader.Close(); }
                        }
                    }

                }
                catch (Exception ex)
                {
                    string x = ex.Message;
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
            return IList;
            

        }


        public string Deactive(string userId)
        {
            return "x";

        }
    }
}