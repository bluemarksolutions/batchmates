﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.DAL
{
    public static class ExecuteSqlCommand
    {

        // Executes the Sql command and return the string from the store procedure
        public static string ExecuteQuery(SqlCommand cmd)
        {
            SqlDataAdapter _sqlda = new SqlDataAdapter();
            _sqlda.SelectCommand = cmd;
            return cmd.ExecuteScalar().ToString();
        }
    }
}