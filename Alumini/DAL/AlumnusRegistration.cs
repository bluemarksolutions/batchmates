﻿using Alumini.BaseClass;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.DAL
{
    public class AlumnusRegistration : ConnectionString
    {
        public AlumnusRegistration(string absoluteURL) : base(absoluteURL)
        {

        }

        SqlConnection con = null;
        public string SaveAlumnusRegistration(Registration oParam)
        {
            string returnValue = string.Empty;

            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand cmd = new SqlCommand("01_alumni_registration", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@first_name", oParam.first_name);
                    cmd.Parameters.AddWithValue("@last_name", oParam.last_name);
                    cmd.Parameters.AddWithValue("@contact_number", oParam.contact_number);
                    cmd.Parameters.AddWithValue("@email", oParam.email);                   
                    cmd.Parameters.AddWithValue("@password", Utilities.Utilities.EncodePassword(oParam.password));
                    cmd.Parameters.AddWithValue("@mode", "save");
                    cmd.Parameters.AddWithValue("@batch", oParam.batch);
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    returnValue = cmd.ExecuteScalar().ToString();
                    return returnValue;
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }
        public string VerifyAlumnus(string email)
        {
            string returnValue = string.Empty;

            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand cmd = new SqlCommand("01_alumni_registration", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@mode", "verify_alumnus");
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    returnValue = cmd.ExecuteScalar().ToString();
                    return returnValue;
                }
            }
            catch (Exception EX)
            {
                return returnValue;
            }
            finally
            {
                con.Close();
            }
        }
        public Registration SignIn(string useremail, string password)
        {
            string returnValue = string.Empty;
            Registration oRegistration = new Registration();

            try
            {
                con = new SqlConnection(this.currentConnectionString);
                using (con)
                {
                    SqlCommand cmd = new SqlCommand("02_alumni_signin", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@email", useremail);
                    cmd.Parameters.AddWithValue("@password", password);
                    con.Open();

                    using (SqlDataReader SDReader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        try
                        {
                            if (SDReader.Read())
                            {
                                oRegistration.alumnus_id = Convert.ToString(SDReader["alumnus_id"]);
                                oRegistration.first_name = Convert.ToString(SDReader["first_name"]);
                                oRegistration.last_name = Convert.ToString(SDReader["last_name"]);
                                oRegistration.email = Convert.ToString(SDReader["email"]);
                            }
                        }
                        catch
                        { throw; }
                        finally
                        { SDReader.Close(); }
                    }

                    return oRegistration;
                }
            }
            catch (Exception EX)
            {
                return oRegistration;
            }
            finally
            {
                con.Close();
            }
        }
    }
}