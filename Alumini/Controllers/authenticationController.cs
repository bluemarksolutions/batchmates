﻿using Alumini.DAL;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Alumini.Controllers
{
    public class authenticationController : Controller
    {
        AlumnusRegistration alumnus = new AlumnusRegistration(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);

        // GET: authentication
        public ActionResult login()
        {
            return View();
        }

        public ActionResult register()
        {
            return View();
        }

        public ActionResult users()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveRegistration(List<Registration> lParam)
        {
            var returnValue = "";
            foreach (Registration oParam in lParam)
            {
                try
                {
                    returnValue = alumnus.SaveAlumnusRegistration(oParam);
                }
                catch (Exception Ex)
                {
                    throw Ex;
                }
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendEmail(string emailid, string firstname, string lastname)
        {
            string body = string.Empty;
            string result = string.Empty;
            try
            {
                //using streamreader for reading my html template   
                using (StreamReader reader = new StreamReader(Server.MapPath("~/EmailTemplates/Confirmation-Email.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{firstname}", firstname);
                body = body.Replace("{lastname}", lastname);
                body = body.Replace("{email}", emailid);

                if (Utilities.Utilities.IsEmptyOrNull(emailid))
                {
                    result = Utilities.Utilities.SendEmail(emailid, "[Batchmates]: Verification link", body);
                }

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        [HttpPost]
        public ActionResult SignIn(FormCollection oObj)
        {
            Registration oParam = new Registration();
            // Assign the Email and Password to UserLoginForm object
            oParam.email = oObj[0];
            oParam.password = Utilities.Utilities.EncodePassword(oObj[1]);

            // authenticates user with provided email and password in login form
            // author:Mukesh(28-May-2019)
            oParam = alumnus.SignIn(oParam.email, oParam.password);

            if (Utilities.Utilities.IsEmptyOrNull(oParam.alumnus_id))
            {
                Session["alumnusid"] = oParam.alumnus_id;
                Session["first_name"] = oParam.first_name;
                Session["last_name"] = oParam.last_name;
                Session["email"] = oParam.email;

                return RedirectToAction("profile", "batchmates");
            }
            else
            {
                oParam.alert = "Email or Password Incorrect";
                return View("login", oParam);
            }
        }

        [HttpGet]
        public ActionResult VerifyAccount()
        {
            return View();
        }

        [HttpPost]
        public JsonResult VerifyAlumnus(string email)
        {
            string result = alumnus.VerifyAlumnus(email);

            // This function verifies account after user clicks on the link in the email
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}