﻿using Alumini.DAL;
using Alumini.Models;
using salespro.Manage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Alumini.Controllers
{
    public class eventsController : Controller
    {
        ManageEvent oManageEvent = new ManageEvent(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        // GET: events
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult list()
        {
            return View();
        }

        //Saves New Event to the database
        //author:Mukesh(14-Mat-2020)
        [HttpPost]
        public JsonResult SaveEvent(List<AlumniEvent> lEvents)
        {
            var returnValue = "";

            foreach (AlumniEvent oEvent in lEvents)
            {
                try
                {
                    oEvent.created_by = Convert.ToString(Session["alumnusid"]);
                    returnValue = oManageEvent.SaveEvent(oEvent);
                }
                catch (Exception Ex)
                {
                    throw Ex;
                }
            }
            return Json(returnValue, JsonRequestBehavior.AllowGet);
        }
    }
}