﻿using Alumini.DAL;
using Alumini.MediatorClass;
using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Alumini.Controllers
{
    public class askquestionsController : Controller
    {
        AskQuestionDL objDAL = new AskQuestionDL(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        utilities objUtil = new utilities(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
        // GET: askquestions
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult list()
        {
            return View();
        }
        [HttpPost]
        //public ActionResult saveaskquestion(string question_number, string question,string EmpID,string quespublish_totion,string urgency,string description,string mode)
        //{    
        //   // AskQuestionDL obj = new AskQuestionDL();
        //    string xyx=  obj.saveaskquestion(question_number,question,EmpID,quespublish_totion,urgency,description,mode);            
        //    return Content("Question added successfully"); 
        //}


        public JsonResult saveaskquestion(List<AskQuestionModel> lParam)
        {
            AskQuestionModel obj = new AskQuestionModel();
            foreach(var item in lParam)
            {
                obj.question_number = item.question_number;
                obj.mode = item.mode;
                obj.question = item.question;
                obj.description = item.description;
                obj.urgency = item.urgency;
                obj.publish_to = item.publish_to;
                obj.userid = item.userid;  
            }           
            string xyx = objDAL.saveaskquestion(obj);
            return Json(xyx, JsonRequestBehavior.AllowGet);
        }

        public JsonResult savegivenanswer(List<Answer> lAnswer)
        {
            Answer objAnswer = new Answer();
            foreach (var item in lAnswer)
            {
                objAnswer.question_number = item.question_number;
                objAnswer.answer = item.answer;
                objAnswer.answer_given_by = item.answer_given_by;
                objAnswer.question_ask_by = item.question_ask_by;
                objAnswer.question_id = item.question_id;                              
            }
            string answers = objDAL.saveanswers(objAnswer);
            return Json(answers, JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        public JsonResult getaskquestion(string ID, string mode)
        {
            //AskQuestionDL obj = new AskQuestionDL();
            List<AskQuestionModel> Ilist = new List<AskQuestionModel>();
            // Ilist = obj.getaskquestion(ID,mode);
            BusinessLayer obj = new BusinessLayer(new AskQuestionDL(System.Web.HttpContext.Current.Request.Url.AbsoluteUri));
            Ilist = obj.GetData(ID, mode);
            return Json(Ilist, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenNum()
        {            
            string GUIDnumber = "";
            GUIDnumber = objUtil.GenerateQuestionNumber();
            return Json(GUIDnumber, JsonRequestBehavior.AllowGet);
        }

      public ActionResult  GetQuestionWithReplyAnswer(string ID,string QNum)
        {
            List<AskQuestionModel> Ilist = new List<AskQuestionModel>();
            Ilist = objDAL.GetQuestionWithReplyAnswer(ID,QNum);
            return Json(Ilist, JsonRequestBehavior.AllowGet);
        }
    }
}