﻿using Alumini.BaseClass;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.Models
{
    public class utilities : ConnectionString
    {

        public utilities(string absoluteURL) : base(absoluteURL)
        {

        }
        public string GenerateQuestionNumber()
        {
            string Number = "";
            int Count = GenerateQueNumber();
            Number = GenerateQuestionNumber(Count, "D5");
            return Number;
        }

        public int GenerateQueNumber()
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection())
            {
                // get connectionstring according to domain name
                conn.ConnectionString = this.currentConnectionString;
                using (SqlCommand cmd = new SqlCommand("sp_askquestion_count", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conn;
                    conn.Open();
                    using (SqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            count = Convert.ToInt32(sdr["id"]);
                        }
                    }
                    conn.Close();
                }
            }
            return count;
        }

        public string GenerateQuestionNumber(int Number, string leadingZeroCount)
        {
            //GenerateLeadNumber
            Number = Number + 1;
            return "Q" + DateTime.Now.Year.ToString().Substring(2, 2) + "-" + string.Format((Number).ToString());
        }
    }
}