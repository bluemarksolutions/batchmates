﻿using Alumini.BaseClass;
using Alumini.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Alumini.Models
{
    public class AlumniEvent
    {
        #region Properties
        public string event_id { get; set; }
        public string event_title { get; set; }
        public string who_can_see { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public string event_description { get; set; }
        public string current_status_id { get; set; }
        public string current_status_name { get; set; }
        public int yes_count { get; set; }
        public int no_count { get; set; }
        public int maybe_count { get; set; }
        public string user_fullname { get; set; }
        public string created_by { get; set; }
        public DateTime created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime modified_on { get; set; }
        #endregion      
    }
}