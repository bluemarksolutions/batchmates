﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alumini.Models
{
    public class Registration
    {
        public int id { get; set; }
        public string alumnus_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string contact_number { get; set; }
        public string email { get; set; }
        public string batch { get; set; }
        public string password { get; set; }
        public string status { get; set; }
        public int status_id { get; set; }
        public bool isactive { get; set; }
        public string created_by { get; set; }
        public string alert { get; set; }
    }
}