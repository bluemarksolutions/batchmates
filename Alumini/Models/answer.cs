﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alumini.Models
{
    public class Answer
    {
        public int answer_id { get; set; }
        public string answer { get; set; }
        public string question_ask_by { get; set; }
        public string answer_given_by { get; set; }
        public int question_id { get; set; }
        public string question_number { get; set; }
        public string answer_status { get; set; }
        public bool isactive { get; set; }
        public string created_by { get; set; }
        public DateTime created_on { get; set; }        
    }
}