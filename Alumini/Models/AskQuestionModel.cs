﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alumini.Models
{
    public class AskQuestionModel
    {
        public int quisttion_id { get; set; }
        public string question_number { get; set; }
        public string question { get; set; }
        public string urgency { get; set; }
        public string publish_to { get; set; }
        public string description { get; set; }
        public string userid { get; set; }
        public string qustion_status { get; set; }
        public string answer_count { get; set; }
        public string view_count { get; set; }
        public bool isactive { get; set; }
        public string created_by { get; set; }
        public string created_by_id { get; set; }        
        public DateTime created_on { get; set; }
        public string modified_by { get; set; }
        public DateTime modified_on { get; set; }
        public string displaycreatedate { get; set; }         
        public string mode { get; set; }
        public string answer { get; set; }
        public string answer_given_on { get; set; }
        public string answer_given_by { get; set; }
        public string answer_given_by_id { get; set; }
        public int count { get; set; }

    }
}