﻿using Alumini.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alumini.Interfaces
{
   public interface ICRUDGerneral
    {
       // string Create(T obj);
       List<AskQuestionModel> GetData(string ID,string Mode);
        // void Update(T obj);
        string Deactive(string ID);
    }

    //interface ICUAskQuestion     {
    //    string SaveAndUpdate(string question_number, string question, string EmpID, string quespublish_totion, string urgency, string description, string mode);       
    //  //  void Update(string question_number, string question, string EmpID, string quespublish_totion, string urgency, string description, string mode);
       
    //}

}
